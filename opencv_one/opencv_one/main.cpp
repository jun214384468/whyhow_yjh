//
//  main.cpp
//  opencv_one
//
//  Created by yh on 2020/11/26.
//  Copyright © 2020 yh. All rights reserved.
//

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, const char * argv[]) {
    // insert code here...
    // 获取图片
    Mat src = imread("/Users/yh/Desktop/whyhow_yjh/opencv_one/opencv_one/img/11.jpg");
    if(src.empty()){
        printf("No image!");
        return -1;
    }
    // 拷贝灰度图
    Mat bb;
    cvtColor(src, bb, CV_BGR2GRAY);
    
    Mat bbb;
    bbb.create(src.size(),src.type());
//    namedWindow("bbb", CV_WINDOW_AUTOSIZE);
//    imshow("bbb", bbb);
    // 读取像素 取反射
    int height = src.rows;
    int width = src.cols;
    int channle = src.channels();
//    cout<<channle<<endl;
    for (int rows = 0;rows<height;rows++){
        for (int cols=0; cols<width; cols++) {
            if(channle==1){
                int gray = bb.at<uchar>(rows,cols);
                bb.at<uchar>(rows,cols) = 255-gray;
            }else if (channle==3)
            {
                int b = src.at<Vec3b>(rows,cols)[0];
                int g = src.at<Vec3b>(rows,cols)[1];
                int r = src.at<Vec3b>(rows,cols)[2];
                bbb.at<Vec3b>(rows,cols)[0] = 255 - b;
                bbb.at<Vec3b>(rows,cols)[1] = 255 - g;
                bbb.at<Vec3b>(rows,cols)[2] = 255 - r;
            }
        }
    }
    // 显示模块
    namedWindow("output", CV_WINDOW_AUTOSIZE);
    imshow("output", src);
    namedWindow("gray", CV_WINDOW_AUTOSIZE);
    imshow("gray", bb);
    namedWindow("fanshe", CV_WINDOW_AUTOSIZE);
    imshow("fanshe", bbb);
    waitKey(0);
    return 0;
}
