//
//  main.cpp
//  shibie
//
//  Created by yh on 2020/11/26.
//  Copyright © 2020 yh. All rights reserved.
//

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


int element_size = 4;

Mat src = imread("/Users/yh/Desktop/22.jpg");
Mat opencc;

// 膨胀操作
Mat cv_dilate(int, void*)
{
    Mat pengzhang;
    int s = element_size * 2 + 1;
    Mat element = getStructuringElement(MORPH_RECT, Size(s,s),Point(-1,-1));
    // 灰度图
    Mat gray,junzhi;
    cvtColor(src, gray, CV_BGR2GRAY);
    blur(gray, junzhi, Size(3,3));
    dilate(junzhi, pengzhang, element);
    imshow("pengzhang", pengzhang);
    return pengzhang;
    
}
// 腐蚀操作
Mat cv_erode(int, void*)
{
    Mat dst;
    int s = element_size*2+1;
    Mat element = getStructuringElement(MORPH_RECT, Size(s,s),Point(-1,-1));
    Mat gray,junzhi;
    cvtColor(src, gray, CV_BGR2GRAY);
    blur(gray, junzhi, Size(3,3));
    erode(junzhi, dst, element);
    imshow("fushi", dst);
    return dst;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    // 读取图片
    if(src.empty())
    {
        printf("No Image!");
        return -1;
    }
    // 灰度图
    Mat gray;
    cvtColor(src, gray, CV_BGR2GRAY);
    // 复制一张原图（测试用）
    Mat copy_img;
    copy_img.create(src.size(),src.type());
    // 取映射值
    int height = src.rows;
    int width = src.cols;
    // 原图通道3
    int channal = src.channels();
    // 灰度图通道1
    //int channal = gray.channels();
    for (int row = 0; row<height;row++){
        for (int col = 0; col<width;col++)
        {
            if(channal==1)
            {
                int bb = gray.at<uchar>(row,col);
                gray.at<uchar>(row,col)= 255-bb;
            }else if(channal == 3)
            {
                int b = src.at<Vec3b>(row,col)[0];
                int g = src.at<Vec3b>(row,col)[1];
                int r = src.at<Vec3b>(row,col)[2];
                copy_img.at<Vec3b>(row,col)[0] = 255-b;
                copy_img.at<Vec3b>(row,col)[1] = 255-g;
                copy_img.at<Vec3b>(row,col)[2] = 255-r;
            }
        }
    }
    // 将反射值转换成灰度
    Mat gray2;
    cvtColor(copy_img, gray2, CV_BGR2GRAY);
    int lala = gray2.channels();
    std::cout<<"channals:"<<lala<<std::endl;
   
    // 均值滤波
    Mat junzhi,junzhi2;
    blur(gray, junzhi, Size(3,3));
//    blur(gray, junzhi2, Size(5,5));
    // 中值滤波
    Mat zhongzhi, zhongzhi2;
    medianBlur(gray, zhongzhi, 3);
//    medianBlur(gray, zhongzhi2, 5);
    
    // 开运算（先膨胀后腐蚀）
    Mat openyunsnuan,openyunsnuan1;
    Mat kernel =  getStructuringElement(MORPH_RECT, Size(25,25),Point(-1,-1));
    morphologyEx(zhongzhi, openyunsnuan, MORPH_OPEN, kernel);
    addWeighted(zhongzhi, 1, openyunsnuan, -1, 0, openyunsnuan);
//    addWeighted(junzhi, 1, openyunsnuan, -1, 0, openyunsnuan1);
//    imshow("open_yunsuan1", openyunsnuan1);
    imshow("open_yunsuan", openyunsnuan);
    // 阈值分割
    Mat fenge;
    threshold(openyunsnuan, fenge, 0, 255, THRESH_BINARY+THRESH_OTSU);
    imshow("fenge", fenge);
    
    // 边缘检测(选用Canny)
    Mat out_canny;
    Canny(fenge, out_canny, 10, 10);
    // Sobal 算子
    Mat resultX, resultY, resultXY;
    //X方向一阶边缘
    Sobel(fenge, resultX, CV_16S, 2, 0, 1);
    convertScaleAbs(resultX, resultX);
    //Y方向一阶边缘
    Sobel(fenge, resultY, CV_16S, 0, 1, 3);
    convertScaleAbs(resultY, resultY);
    //整幅图像的一阶边缘
    resultXY = resultX + resultY;
    imwrite("/Users/yh/Desktop/result.jpg", resultXY);
    
    // 对XXX进行膨胀操作
//    namedWindow("pengzhang", CV_WINDOW_AUTOSIZE);
//    createTrackbar("Element Size", "pengzhang", &element_size, max_size,cv_dilate);
    // 对XXX进行腐蚀操作
//    namedWindow("fushi", CV_WINDOW_AUTOSIZE);
//    createTrackbar("Element Size:", "fushi", &element_size, max_size, cv_erode);
    
    // 显示结果
//    namedWindow("original", CV_WINDOW_AUTOSIZE);
//    imshow("original", src);
//    namedWindow("fanshe", CV_WINDOW_AUTOSIZE);
////    imshow("fanshe", copy_img);
//    namedWindow("gray", CV_WINDOW_AUTOSIZE);
//    imshow("gray", gray);
//    namedWindow("gray2", CV_WINDOW_AUTOSIZE);
//    imshow("cgray2", gray2);
    namedWindow("out_canny",CV_WINDOW_AUTOSIZE);
    imshow("out_canny", out_canny);
//    namedWindow("out_canny2",CV_WINDOW_AUTOSIZE);
//    imshow("out_canny2", out_canny2);
    namedWindow("sobal",CV_WINDOW_AUTOSIZE);
    imshow("sobal", resultXY);
    
//    namedWindow("junzhi", CV_WINDOW_AUTOSIZE);
//    imshow("junzhi", junzhi);
//    namedWindow("junzhi2", CV_WINDOW_AUTOSIZE);
//    imshow("junzhi2", junzhi2);
//    imshow("zhongzhi", zhongzhi);
//    imshow("zhongzhi2", zhongzhi2);
    
    waitKey(0);
    
    return 0;
}


