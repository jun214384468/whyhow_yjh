//
//  main.cpp
//  LicenseDetection
//
//  Created by yh on 2020/12/2.
//  Copyright © 2020 www.whyhowinfo.com. All rights reserved.
//

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>
#include <sys/malloc.h>

using namespace std;
using namespace cv;

/*******************************
 >Copyright (c++),2020,WhyHow info ,All right reserverd
 >Author : junhongyan
 >File Name:License_detection
 >Created Time: 2020.11.30
 >Descraption: opencv实现简单的车牌识别
 >Function list: License detection
 **********************************/

// 声明
Mat contour(Mat gray, vector<vector<Point>> &contours, vector<Vec4i> &hierarchy, Mat img);
Point2f(*choose_contour(vector<vector<Point>> contours))[2];
int license_gain(Point2f (*choose_license)[2], Mat img);

int choose_license_sum(Point2f (*choose_license)[2]);

Mat License_cut(Mat img);

/*********************主函数***************************/
int main(int argc, const char * argv[]) {
    // insert code here...
    // 读取图片信息
    Mat originale_img = imread("/Users/yh/Desktop/22.jpg");
    // 验证图片是否读取成功
    if (originale_img.empty()) {
        printf("No Image!");
        return -1;
    }
    
    // 转换成灰度图
    Mat img2gray;
    cvtColor(originale_img, img2gray, CV_BGR2GRAY);
    
    // 得出轮廓
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    Mat show_img = contour(img2gray, contours, hierarchy, originale_img);
    imshow("ShowImg", show_img);
    
    License_cut(show_img);
    
    waitKey(0);
    
    return 0;
}

/*********************提取轮廓边缘***************************/
/******************************
 Function: contour()
 Descraption: 获取车牌的大致位置
 Calls: none
 Calls By: main()
 Input: img2gray
 Output: true
 Return: none
 Author: junhongyan
 History: none
 Others: none
 *******************************/
Mat contour(Mat gray, vector<vector<Point>> &contours, vector<Vec4i> &hierarchy, Mat img)
{
    // 中值滤波
    Mat median_filter;
    medianBlur(gray, median_filter, 5);
    
    // 开运算
    Mat open_operation;
    Mat kernel = getStructuringElement(MORPH_RECT, Size(20,20),Point(-1,-1));
    morphologyEx(median_filter, open_operation, MORPH_OPEN, kernel);
    
    // 加权
    addWeighted(median_filter, 1, open_operation, -1, 0, open_operation);
    
    // 阈值分割
    Mat threshold_segmentation;
    threshold(open_operation, threshold_segmentation, 0, 255, THRESH_BINARY+THRESH_OTSU);
    
    // 边缘检测 sobal 算子
    Mat sobal_x,sobal_y,sobal_xy;
    // X方向一阶边缘
    Sobel(threshold_segmentation, sobal_x, CV_16S, 2, 0, 1);
    convertScaleAbs(sobal_x, sobal_x);
    // Y方向一阶边缘
    Sobel(threshold_segmentation, sobal_y, CV_16S, 0, 1,3);
    convertScaleAbs(sobal_y, sobal_y);
    // 整张图片的一阶边缘
    sobal_xy = sobal_y+sobal_x;
    
    // 闭合+开合
    kernel = getStructuringElement(MORPH_RECT, Size(29,29));
    morphologyEx(sobal_xy, sobal_xy, MORPH_CLOSE, kernel);
    morphologyEx(sobal_xy, sobal_xy, MORPH_OPEN, kernel);
    
    // 平滑处理
    Mat blurr_image;
    medianBlur(sobal_xy, blurr_image, 9);
    medianBlur(blurr_image, blurr_image, 9);
    //imshow("Blur_image", blurr_image);
    
    
    // 寻找边缘点
    findContours(blurr_image, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point());
    // 画出轮廓
    //drawContours(img, contours, -1, Scalar(255), 1);
    Mat roi_image, number_img;
    vector<Point> rectPoint;
    for (int i = 0; i<contours.size(); i++)
    {
        
        Rect r = boundingRect(Mat(contours[i]));
        cout<<"contours"<<i<<"height = "<<r.height<<"width = "<<r.width<<"rate = "<<((float)r.width/r.height)<<endl;
        // 2.2。3.6
        if((float)r.width /r.height>=2.2 && (float)r.width/r.height<=3.5)
        {
            
            number_img = img(r);
            rectangle(img, contours[i][0], contours[i][1], Scalar(0,0,255),2);
            imwrite("/Users/yh/Desktop/result/"+typeToString(i)+".jpg", number_img);
            
            for(int j = 0;j<contours[i].size();j++)
            {
                cout<<"point = "<<contours[i][j]<<endl;
            }
            roi_image = img(r);
        }
    }
    return number_img;
}


/*******************进行车牌切割************************/
/**************************
 Function:License_cut()
 Descraption:对获取到的车牌进行切割处理
 Calls: none
 Calls By: main()
 Input: img2gray
 Output: true
 Return: none
 Author: junhongyan
 History: none
 Others: none
 **************************/
Mat License_cut(Mat img)
{
    Mat out_img;  //占坑
    // 灰度处理
    Mat img2gray;
    cvtColor(img, img2gray, CV_BGR2GRAY);
    
    // 高斯模糊
    Mat gaosi_blur;
    GaussianBlur(img2gray, gaosi_blur, Size(3, 3), 0, 0);
    
    // 阈值分割
    Mat seg_img;
    threshold(gaosi_blur, seg_img, 0, 255, THRESH_BINARY+THRESH_OTSU);
    
    // 边缘检测与轮廓提取
    Mat canny_detection;
    Canny(seg_img, canny_detection, 200, 100);
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    findContours(canny_detection, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE,Point());
    int size = (int)(contours.size());
    
    // 保存符合边框的符号
    vector<int> num_order;
    map<int, int> num_map;
    
    for (int i = 0; i < size; i++) {
        // 获取边框数据
        Rect number_rect = boundingRect(contours[i]);
        int width = number_rect.width;
        int height = number_rect.height;
        // 去除较小的干扰边框，筛选出合适的区域
        if (width > img.cols/10 && height > img.rows/2) {
            rectangle(seg_img, number_rect.tl(), number_rect.br(), Scalar(255, 255, 255), 1, 1, 0);
            num_order.push_back(number_rect.x);
            num_map[number_rect.x] = i;
        }
    }
    // 按符号顺序提取
    sort(num_order.begin(), num_order.end());
    for (int i = 0; i < num_order.size(); i++) {
        Rect number_rect = boundingRect(contours[num_map.find(num_order[i])->second]);
        Rect choose_rect(number_rect.x, 0, number_rect.width, img2gray.rows);
        Mat number_img = img2gray(choose_rect);
        imshow("number" + to_string(i), number_img);
        // imwrite("number" + to_string(i) + ".jpg", number_img);
    }
   // imshow("添加方框", img2gray);
   // waitKey(0);

    return out_img;
}
